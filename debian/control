Source: pydantic
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Michael Banck <mbanck@debian.org>,
           Colin Watson <cjwatson@debian.org>
Section: python
Testsuite: autopkgtest-pkg-pybuild
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python (>= 6.20241024~),
               dh-sequence-python3,
               cython3,
               pybuild-plugin-pyproject,
               python3-all-dev,
               python3-annotated-types <!nocheck>,
               python3-cloudpickle <!nocheck>,
               python3-dirty-equals <!nocheck>,
               python3-dotenv <!nocheck>,
               python3-email-validator (>= 2.0) <!nocheck>,
               python3-fake-factory <!nocheck>,
               python3-hatch-fancy-pypi-readme,
               python3-jsonschema <!nocheck>,
               python3-packaging (>= 23.2) <!nocheck>,
               python3-pydantic-core (>= 2.27.2),
               python3-pydantic-core (<< 2.27.3~),
               python3-pytest (>= 8.2.2) <!nocheck>,
               python3-pytest-benchmark <!nocheck>,
               python3-pytest-mock <!nocheck>,
               python3-rich <!nocheck>,
               python3-setuptools,
               python3-typing-extensions (>= 4.12.2) <!nocheck>,
               python3-tz <!nocheck>,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/python-team/packages/pydantic
Vcs-Git: https://salsa.debian.org/python-team/packages/pydantic.git
Homepage: https://github.com/samuelcolvin/pydantic
Rules-Requires-Root: no

Package: python3-pydantic
Architecture: any
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
         python3-email-validator
Breaks: python3-dirty-equals (<< 0.8~),
        python3-djantic (<< 0.7.0-5~),
        python3-maison (<< 2~),
        python3-pydantic-settings (<< 2.5.0),
Description: Data validation and settings management using Python type hinting
 pydantic uses type hinting (PEP 484) and variable annotation (PEP 526) to
 validate that untrusted data takes the desired form. There is also support for
 an extension to dataclasses where the input data is validated.
